---
layout: page
title: About
title_kannada: ನಮ್ಮ ಬಗ್ಗೆ
permalink: /about/
nav: true
---

<!--FSMK is a registered not-for-profit organization whose primary objective is to spread and create awareness about free software technologies amongst different sections of the society. FSMK is driven by volunteers from various backgrounds like software professionals, government officials, academicians and students. The Students Chapter of FSMK works actively with college students and encourages them not only to use free software but also contribute to the development of various free software. We facilitate formation of college-wise units called GNU/Linux User's Group (GLUG) which conduct various technical sessions regularly throughout the academic session.-->

<!--“Free software” means software that respects users' freedom and community. Roughly, it means that the users have the freedom to run, copy, distribute, study, change and improve the software. Thus, “free software” is a matter of liberty, not price. To understand the concept, you should think of “free” as in “free speech,” not as in “free lunch”.-->

<!--We campaign for these freedoms because everyone deserves them. With these freedoms, the users (both individually and collectively) control the program and what it does for them. When users don't control the program, we call it a “nonfree” or “proprietary” program. The nonfree program controls the users, and the developer controls the program; this makes the program an instrument of unjust power.-->

Free Software Movement Karnataka is a registered not-for-profit organization. Our primary objective is to create and spread awareness of Free Software technologies in different strata of society. We are driven by volunteers, who by day are software engineers, students, academicians, or government officials, and by night are Free Software evangelists.

We work closely with college students and encourage them to use, and help develop, Free Software. To make this student interaction, we set up college-level units called GNU/Linux User Groups (GLUGs). These GLUGs organise various technical sessions and events.

But what is Free Software? Free Software is software that respects a user's freedoms. In brief, it means that all users have the freedom to run, copy, distribute, study, change, and improve the software as they see fit. Think Free as in Free Speech, not Free Biryani.

The opposite of Free Software is what is called Proprietary Software. Proprietary Software do not come with the same freedoms granted to the user by the developers of Free Software. Instead of the user controlling the software, often the software ends up controlling the user, especially since there is no way for the user to even verify what the software does. Proprietary Software is thus an instrument of unjust power, weilded by its developers.
