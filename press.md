---
layout: dynamic_page
title: Press
title_kannada:
permalink: /press/
---

<div class="row row-margin">
  {% for post in site.categories.press %}
  {% if post.language != 'ka' %}
  {% include small_card.html %}
  {% endif %}
  {% endfor %}
</div>
