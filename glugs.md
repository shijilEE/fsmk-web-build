---
layout: page
title: GLUGs
title_kannada: ಗ್ಲಗ್
permalink: /glugs/
nav: true
---

GNU/Linux user groups are the means of spreading the idea of free software, hardware, and philosphy in campuses. GLUGs serve as a base to technically empower yourself and interact with professionals, right from installation of Linux to hacking the kernel, web technologies like drupal and mastering the latest python module, at the same time raising questions of self reliance. Most of our active volunteers started out as GLUG members. We have over 20 GLUGs across the state and are constantly working on starting GLUGs in more colleges.

Our GLUG activites could be summarised as

  +  Increasing knowledge of free software /hardware technologies.

  +  Introducing people to the free software/hardware community.

  +  Providing a platform to discuss technology and its philosophy.

  +  Mentoring new members with new projects, both on a local and international scale.

  +  Conducting workshops and seminars to increase curiosity in these technologies.


Starting a new GLUG is as easy as inviting any FSMK member to the college/institute/company you belong and arrange for session. Followed by session we help you form a GLUG and guide you further.

Some interesting discussions about GLUGs could be found here:

<a href="https://discuss.fsmk.org/t/what-is-the-purpose-of-a-glug">https://discuss.fsmk.org/t/what-is-the-purpose-of-a-glug</a>

<a href="https://discuss.fsmk.org/t/how-do-we-sustain-a-glug">https://discuss.fsmk.org/t/how-do-we-sustain-a-glug</a>
