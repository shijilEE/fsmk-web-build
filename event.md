---
layout: dynamic_page
title: Events
title_kannada: ಸುದ್ದೆ
permalink: /event/
---

<div class="row row-margin">
  {% for post in site.categories.event %}
  {% if post.language != 'ka' %}
  {% include small_card.html %}
  {% endif %}
  {% endfor %}
</div>
